module Lib (someFunc, firstHaskellFunction, closure) where


-- firstHaskellFunction:: Integer Integer
closure :: Int -> Int -> Int 
closure x = f
    where
    f y = x + y



firstHaskellFunction = show . succ

someFunc :: IO ()
someFunc = do
     putStrLn "some doo Func"
     putStrLn (firstHaskellFunction 8)
