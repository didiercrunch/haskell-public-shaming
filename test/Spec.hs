-- Tests written with https://hspec.github.io/

import Test.Hspec
import Lib


main :: IO ()
main = hspec $ do
    describe "Prelude.head" $ do
        it "returns the first element of a list" $ do
            head [23 ..] `shouldBe` (23 :: Int)
    describe  "my first function" $ do
        it "should increment a number and return a string" $ do
            firstHaskellFunction 78 `shouldBe` "79"
    describe  "my first closure" $ do
        it "do something use ful" $ do
            (closure 8) 10 `shouldBe` 18
